﻿using Mono.Cecil;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnityCecil
{
    class Program
    {
        static void Main(string[] args)
        {
            Program pg = new Program();
            pg.AddGameObjectModifications("C:/Temp/ue.dll");
            //var temp = Console.Read();
            Console.WriteLine("Modifications finished, new DLL placed in C:/Temp/UnityEngine.new.dll.");
        }

        public void AddGameObjectModifications(string fileName)
        {
            ModuleDefinition module = ModuleDefinition.ReadModule(fileName);

            TypeDefinition goClass = module.Types.Single(typedef => typedef.FullName == "UnityEngine.GameObject");

            FieldDefinition Tags = new FieldDefinition("Tags", FieldAttributes.Public, module.ImportReference(typeof(long)));

            Tags.InitialValue = new byte[]{ 0 };

            goClass.Fields.Add(Tags);

            FieldDefinition objCache = new FieldDefinition("ObjCache", FieldAttributes.Public, module.ImportReference(typeof(Dictionary<int, object>)));
            goClass.Fields.Add(objCache);

            //foreach (TypeDefinition type in module.Types) {
            //    if (!type.IsPublic)
            //        continue;
            //
            //    if (type.FullName.Equals("UnityEngine.GameObject"))
            //        Console.WriteLine(type.FullName);
            //}

            module.Write("C:/Temp/UnityEngine.new.dll");
        }
    }
}
